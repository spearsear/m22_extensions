<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 *
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpAssignProduct\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Model\Session;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\Registry;

class AssignSeller implements ObserverInterface
{
    /**
     * @var \Webkul\MpAssignProduct\Helper\Data
     */
    protected $_assignHelper;

    protected $_response;

    protected $_url;
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;

    /**
     * Core registry.
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var SellerProduct
     */
    protected $_sellerProductCollectionFactory;
    /**
     * @var CollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @param \Webkul\MpAssignProduct\Helper\Data $helper
     */
    public function __construct(
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Webkul\MpAssignProduct\Helper\Data $helper,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Framework\UrlInterface $url,
        Session $customerSession,
        Registry $coreRegistry,
        CollectionFactory $productCollectionFactory,        
        \Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory  $sellerProductCollectionFactory
    )
    {
        $this->_messageManager = $messageManager;
        $this->_assignHelper = $helper;
        $this->_response = $responseFactory;
        $this->_url = $url;
        $this->_coreRegistry = $coreRegistry;
        $this->_customerSession = $customerSession;
        $this->_productCollectionFactory = $productCollectionFactory;        
        $this->_sellerProductCollectionFactory = $sellerProductCollectionFactory;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $params = $observer->getData();
        $ids = [];
        if (array_key_exists(0, $params)) {
            $sellerId = $this->_customerSession->getCustomerId();
            if (array_key_exists('id', $params[0])) {
                $productId = $params[0]['id'];
                if (!$this->_assignHelper->hasAssignedProducts($productId)) {
                    return;
                }
                $sellerProducts = $this->_sellerProductCollectionFactory
                                ->create()
                                ->addFieldToFilter(
                                    'mageproduct_id',
                                    $productId
                                )->addFieldToFilter(
                                    'seller_id',
                                    $sellerId
                                );
                if ($this->_customerSession->getAssignProductIds()) {
                    $ids = $this->_customerSession->getAssignProductIds();
                }
                if ($sellerProducts->getSize()) {
                    $ids[] = $productId;
                    $this->_customerSession->setAssignProductIds($ids);
                    $this->_assignHelper->assignSeller($productId);
                }
            }
        }
    }
}