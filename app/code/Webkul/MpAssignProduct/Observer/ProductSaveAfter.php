<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAssignProduct
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpAssignProduct\Observer;

use Magento\TestFramework\ErrorLog\Logger;
use Magento\Catalog\Model\Product;
use Magento\Framework\Event\ObserverInterface;

/**
 * Webkul MpAssignProduct ProductSaveAfter Observer.
 */
class ProductSaveAfter implements ObserverInterface
{

    protected $_assignHelper;

    public function __construct(
        \Webkul\MpAssignProduct\Helper\Data $helper,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
    ) {
        $this->_assignHelper=$helper;
        $this->_stockRegistry = $stockRegistry;
    }

    /**
     * Product delete after event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $productId = $observer->getProduct()->getId();
        $assignQty = $this->_assignHelper->getAssignProductQty($productId,true);
        $product = $this->_assignHelper->getProduct($productId);
        if ($product->getTypeId() == 'simple' || $product->getTypeId() == 'virtual') {
            $stockItem = $this->_stockRegistry->getStockItem($productId);
            $qty = $stockItem->getQty();
            $qty = $qty + $assignQty;
            $stockItem->setData('qty', $qty);
            $stockItem->setIsInStock((bool)$qty);
            $stockItem->save();
        }
    }
}