<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Webkul\MpAssignProduct\Observer;

use Magento\CatalogInventory\Api\StockConfigurationInterface;
use Magento\CatalogInventory\Api\StockManagementInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

/**
 * Catalog inventory module observer
 */
class RefundOrderInventoryObserver implements ObserverInterface
{
    /**
     * @var StockConfigurationInterface
     */
    protected $stockConfiguration;

    /**
     * @var StockManagementInterface
     */
    protected $stockManagement;

    /**
     * @var \Magento\CatalogInventory\Model\Indexer\Stock\Processor
     */
    protected $stockIndexerProcessor;

    /**
     * @var \Magento\Catalog\Model\Indexer\Product\Price\Processor
     */
    protected $priceIndexer;

    /**
     * @var OrderLoaderInterface
     */
    protected $_orderLoader;

    protected $_mpassignItem;

    protected $_mpassignHelper;

    /**
     * @param StockConfigurationInterface $stockConfiguration
     * @param StockManagementInterface $stockManagement
     * @param \Magento\CatalogInventory\Model\Indexer\Stock\Processor $stockIndexerProcessor
     * @param \Magento\Catalog\Model\Indexer\Product\Price\Processor $priceIndexer
     */
    public function __construct(
        StockConfigurationInterface $stockConfiguration,
        StockManagementInterface $stockManagement,
        \Magento\Sales\Api\Data\OrderInterface $orderLoader,
        \Magento\CatalogInventory\Model\Indexer\Stock\Processor $stockIndexerProcessor,
        \Magento\Catalog\Model\Indexer\Product\Price\Processor $priceIndexer,
        \Webkul\MpAssignProduct\Model\ItemsFactory $itemsFactory,
        \Webkul\MpAssignProduct\Helper\Data $helper
    ) {
        $this->_orderLoader = $orderLoader;
        $this->stockConfiguration = $stockConfiguration;
        $this->stockManagement = $stockManagement;
        $this->stockIndexerProcessor = $stockIndexerProcessor;
        $this->priceIndexer = $priceIndexer;
        $this->_mpassignItem = $itemsFactory;
        $this->_mpassignHelper = $helper;
    }

    /**
     * Return creditmemo items qty to stock
     *
     * @param EventObserver $observer
     * @return void
     */
    public function execute(EventObserver $observer)
    {
        /* @var $creditmemo \Magento\Sales\Model\Order\Creditmemo */
        $creditmemo = $observer->getEvent()->getCreditmemo();
        $itemsToUpdate = [];
        $orderId = $creditmemo->getOrderId();
        $order = $this->_orderLoader->load($orderId);
        $orderItems = $order->getItemsCollection();
        $mpassignProduct = [];
        foreach ($orderItems as $item) {
            $productOptions = $item->getProductOptions();
            if (array_key_exists('mpassignproduct_id',$productOptions['info_buyRequest'])) {
                $mpassignProduct[$productOptions['info_buyRequest']['product']] = $productOptions['info_buyRequest'];
            }
        }
        
        if (count($mpassignProduct)) {
            foreach ($creditmemo->getAllItems() as $item) {
                $qty = $item->getQty();
                $productId = $item->getProductId();
                if (($item->getBackToStock() && $qty) || $this->stockConfiguration->isAutoReturnEnabled()) {
                    if (array_key_exists($productId, $mpassignProduct) && (array_key_exists('mpassignproduct_id', $mpassignProduct[$productId]))) {
                        if (isset($mpassignProduct[$productId]['associate_id'])) {
                            $associate_id = $mpassignProduct[$productId]['associate_id'];
                            $loadItem = $this->_mpassignHelper->getAssociatedItem($associate_id);
                            $loadItem->setQty($loadItem->getQty()+$qty);
                            $loadItem->save();
                        } else {
                            $assignId = $mpassignProduct[$productId]['mpassignproduct_id'];
                            if ($assignId) {
                                $loadItem = $this->_mpassignHelper->getAssignProduct($assignId);
                                $loadItem->setQty($loadItem->getQty()+$qty);
                                $loadItem->save();
                            }
                        }
                    }
                }
            }
        } 
    }
}
