<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAssignProduct
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpAssignProduct\Model\Plugin;

use Magento\Catalog\Model\Product as CatalogProduct;

class Product
{
    /**
     * @var \Webkul\MpAssignProduct\Helper\Data
     */
    private $_helper;

    /**
     * Initialize dependencies.
     *
     * @param \Webkul\Preorder\Helper\Data $preorderHelper
     */
    public function __construct(
        \Webkul\MpAssignProduct\Helper\Data $helper
    ) {
        $this->_helper = $helper;
    }
    
    public function afterIsSalable(CatalogProduct $subject, $result)
    {
        $productId = $subject->getId();

        $allProducts = $this->_helper->getAssignProductCollection($productId);
        if (count($allProducts) > 0) {
            return true;
        }
        return $result;
    }
}
