<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAssignProduct
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpAssignProduct\Plugin\Marketplace\Block\Product;

class Productlist
{
    protected $_customerSession;

    protected $_objectManager;
    /**
     * Initialize dependencies.
     *
     * @param \Webkul\MpAssignProduct\Helper\Data $helper
     */
    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\ObjectManagerInterface $objectManager
    ) {
        $this->_customerSession = $customerSession;
        $this->_objectManager = $objectManager;
    }
    public function aroundGetSalesdetail(
        \Webkul\Marketplace\Block\Product\Productlist $subject,
        \Closure $proceed,
        $productId = ''
    ) {
        $data = [
            'quantitysoldconfirmed' => 0,
            'quantitysoldpending' => 0,
            'amountearned' => 0,
            'clearedat' => 0,
            'quantitysold' => 0,
        ];
        $sum = 0;
        $arr = [];
        if (!($customerId = $this->_customerSession->getCustomerId())) {
            return false;
        }
        $quantity = $this->_objectManager->create(
            'Webkul\Marketplace\Model\Saleslist'
        )
        ->getCollection()
        ->addFieldToFilter('seller_id', $customerId)
        ->addFieldToFilter(
            'mageproduct_id',
            $productId
        );

        foreach ($quantity as $rec) {
            $status = $rec->getCpprostatus();
            $data['quantitysold'] = $data['quantitysold'] + $rec->getMagequantity();
            if ($status == 1) {
                $data['quantitysoldconfirmed'] = $data['quantitysoldconfirmed'] + $rec->getMagequantity();
            } else {
                $data['quantitysoldpending'] = $data['quantitysoldpending'] + $rec->getMagequantity();
            }
        }

        $amountearned = $this->_objectManager->create('Webkul\Marketplace\Model\Saleslist')
                        ->getCollection()
                        ->addFieldToFilter(
                            'cpprostatus',
                            \Webkul\Marketplace\Model\Saleslist::PAID_STATUS_PENDING
                        )
                        ->addFieldToFilter('seller_id', $customerId)
                        ->addFieldToFilter(
                            'mageproduct_id',
                            $productId
                        );
        foreach ($amountearned as $rec) {
            $data['amountearned'] = $data['amountearned'] + $rec['actual_seller_amount'];
            $arr[] = $rec['created_at'];
        }
        $data['created_at'] = $arr;

        return $data;
    }
}