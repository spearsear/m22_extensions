<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAssignProduct
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpAssignProduct\Plugin\Checkout\Controller\Cart;

use Magento\Checkout\Controller\Cart\Configure as CartConfigure;
use Magento\Framework\App\Response\Http as responseHttp;
use Magento\Framework\UrlInterface;

class Configure
{
    /**
     * @var \Webkul\MpAssignProduct\Helper\Data
     */
    private $_mpassignHelper;

    /**
     * Initialize dependencies.
     *
     * @param \Webkul\MpAssignProduct\Helper\Data $preorderHelper
     */
    public function __construct(
        \Webkul\MpAssignProduct\Helper\Data $mpassignHelper,
        responseHttp $response,
        UrlInterface $url,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        $this->response = $response;
        $this->_url = $url;
        $this->_mpassignHelper = $mpassignHelper;
        $this->_messageManager = $messageManager;
    }

    public function afterExecute(
        CartConfigure $subject,
        $result
    ) {
        // print_r($subject->getRequest()->getParams()); die;
        $itemId = (int)$subject->getRequest()->getParam('id');
        $assignData = $this->_mpassignHelper->getAssignDataByItemId($itemId);
        if ($assignData['assign_id']!=0) {
            $url = $this->_url->getUrl('checkout/cart');
            $this->response->setRedirect($url);
            $this->_messageManager->addError("You can not edit Assigned Products.");
            return $result;
        }
        return $result;
    }
}