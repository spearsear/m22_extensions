<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAssignProduct
 * @author    Webkul
 * @copyright Copyright (c) 2010-2016 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpAssignProduct\Plugin\Checkout\CustomerData;

class AbstractItem
{
    /**
     * @var \Webkul\MpAssignProduct\Helper\Data
     */
    private $_helper;

    /**
     * Initialize dependencies.
     *
     * @param \Webkul\MpAssignProduct\Helper\Data $helper
     */
    public function __construct(
        \Webkul\MpAssignProduct\Helper\Data $helper
    ) {
        $this->_helper = $helper;
    }

    public function afterGetItemData(
        \Magento\Checkout\CustomerData\AbstractItem $subject,
        $result
    ) {
        if (isset($result['item_id'])) {
            $item_id = $result['item_id'];
            $assignData = $this->_helper->getAssignDataByItemId($item_id);
            if ($assignData['assign_id'] > 0) {
                $result['is_visible_in_site_visibility']=false;
            }
        }
        return $result;
    }
}
