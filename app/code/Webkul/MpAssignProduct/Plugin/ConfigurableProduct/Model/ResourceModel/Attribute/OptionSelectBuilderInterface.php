<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAssignProduct
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpAssignProduct\Plugin\ConfigurableProduct\Model\ResourceModel\Attribute;

use Magento\ConfigurableProduct\Model\ResourceModel\Attribute\OptionSelectBuilderInterface as OptionSelectBuilder;

class OptionSelectBuilderInterface
{
    public function __construct(
        \Magento\Framework\Registry $coreRegistry
    ) {
        $this->_coreRegistry = $coreRegistry;
    }

    public function afterGetSelect(
        OptionSelectBuilder $subject,
        $result
    ) {
        if ($currentProduct = $this->_coreRegistry->registry('current_product')) {
            $result = str_replace('stock.stock_status = 1', 'stock.stock_status >= 0', $result);
        }
        return $result;
    }
}
