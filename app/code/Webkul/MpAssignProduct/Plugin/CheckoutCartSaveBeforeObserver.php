<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAssignProduct
 * @author    Webkul
 * @copyright Copyright (c) 2010-2016 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpAssignProduct\Plugin;

use Magento\Checkout\Model\Session as CheckoutSession;
use Webkul\Marketplace\Helper\Data as MarketplaceHelperData;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Message\ManagerInterface;

class CheckoutCartSaveBeforeObserver
{
    protected $_logger;

    /**
     * @var \Webkul\MpAssignProduct\Helper\Data
     */
    private $_helper;
    /**
     * @var ManagerInterface
     */
    private $_messageManager;
    /**
     * @var CheckoutSession
     */
    protected $_checkoutSession;

    /**
     * @var MarketplaceHelperData
     */
    protected $_marketplaceHelperData;

    protected $request;
    /**
     * @var ProductRepositoryInterface
     */
    protected $_productRepository;

    protected $_mpAssignQuote;

    /**
     * Initialize dependencies.
     *
     * @param \Webkul\MpAssignProduct\Helper\Data $helper
     */
    public function __construct(
        CheckoutSession $checkoutSession,
        \Webkul\MpAssignProduct\Helper\Data $helper,
        \Webkul\MpAssignProduct\Model\QuoteFactory $mpAssignQuote,
        \Magento\Framework\App\Request\Http $request,
        ManagerInterface $messageManager,
        MarketplaceHelperData $marketplaceHelperData,
        ProductRepositoryInterface $productRepository,
        \Psr\Log\LoggerInterface $logger        
    ) {
	$this->_logger = $logger;
        $this->_checkoutSession = $checkoutSession;
        $this->_helper = $helper;
        $this->request = $request;
        $this->_marketplaceHelperData = $marketplaceHelperData;
        $this->_productRepository = $productRepository;
        $this->_mpAssignQuote = $mpAssignQuote;
        $this->_messageManager = $messageManager;        
    }

    public function aroundExecute(
        \Webkul\Marketplace\Observer\CheckoutCartSaveBeforeObserver $subject,
        \Closure $proceed,
        \Magento\Framework\Event\Observer $observer
    ) {
        if ($this->_marketplaceHelperData->getAllowProductLimit()) {
            $data = $this->request->getParams();
            $items =  $this->_checkoutSession->getQuote()->getAllVisibleItems();
            $quoteId = $this->_checkoutSession->getQuoteId();
            foreach ($items as $item) {
                $mpProductCartLimit=0;
                $assign_product = 0;
                $product = $this->_productRepository->getById($item->getProductId());
                $productTypeId = $product['type_id'];
                if ($productTypeId != 'downloadable' && $productTypeId != 'virtual') {
                    $collection = $this->_mpAssignQuote->create()->getCollection()
                                ->addFieldToFilter('item_id',$item->getId())
                                ->addFieldToFilter('quote_id',$quoteId);
                    if ($collection->getSize()) {
                        foreach ($collection as $key) {
                            $assign_product = $key->getAssignId();
                        }
                    } 
                    if (!(array_key_exists("mpassignproduct_id",$data) || $assign_product)) {
                        $mpProductCartLimit = $product['mp_product_cart_limit'];
                    }
                    if (!$mpProductCartLimit) {
                        $mpProductCartLimit = $this->_marketplaceHelperData->getGlobalProductLimitQty();
                    }
                    if ($item->getQty() > $mpProductCartLimit) {
                        $item->setQty($mpProductCartLimit);
                        $productName = "<b>".$item->getName()."</b>";
                        $this->_messageManager->addError(
                            __(
                                'Sorry, but you can only add maximum %1 quantity of %2 in this cart.',
                                $mpProductCartLimit,
                                $productName
                            )
                        );
                        $this->_checkoutSession->getQuote()->setHasError(true);
                    }
                }
            }
        }
        return $this;
    }
}
