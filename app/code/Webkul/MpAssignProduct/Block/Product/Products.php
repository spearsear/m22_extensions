<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAssignProduct
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpAssignProduct\Block\Product;
//shuping
use Cminds\MultiUserAccounts\Helper\CustomerTree;

class Products extends \Magento\Framework\View\Element\Template
{
    /**
     * shuping
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * shuping
     * @var \Cminds\MultiUserAccounts\Helper\CustomerTree
     */
    protected $_customerTree;

    /**
     * @var \Webkul\MpAssignProduct\Helper\Data
     */
    protected $_assignHelper;

    /**
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Webkul\MpAssignProduct\Helper\Data $helper
     * @param \Cminds\MultiUserAccounts\Helper\CustomerTree $customerTree
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Webkul\MpAssignProduct\Helper\Data $helper,
        \Cminds\MultiUserAccounts\Helper\CustomerTree $customerTree,
        \Psr\Log\LoggerInterface $logger,
        array $data = []
    ) {
        $this->_coreRegistry = $context->getRegistry();
        $this->_request = $context->getRequest();
        $this->_assignHelper = $helper;
        $this->_customerTree = $customerTree;
        $this->_logger = $logger;
        parent::__construct($context, $data);
    }

    /**
     * @return bool|\Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function getAssignedProducts()
    {
        $productId = $this->getProduct()->getId();
        $collection = $this->_assignHelper->getAssignProductCollection($productId)
                            ->addFieldToFilter('main_table.status',1);

        //shuping filtering assign product:
        //  1: only customer logged in can vew assignedProduct
        //  2: logged in customer can only view assignedProduct with seller_id = rootcustomer_id
        if ($this->_assignHelper->customerLoggedIn()) {
           //$rootCustomerId = $this->_customerTree->getRootCustomerId($this->_assignHelper->shupingGetCustomerId());
           //getCustomerId only works when cacheable="false" in block
           $rootCustomerId = $this->_customerTree->getRootCustomerId($this->_assignHelper->getCustomerId());
	   $this->_logger->info("filter assignedProduct by seller_id = " . $rootCustomerId);
           $collection->addFieldToFilter("main_table.seller_id", $rootCustomerId);
        } else {
	   $this->_logger->info("filter assignedProduct by product_id = " . "9999999999999");
           $collection->addFieldToFilter("product_id", "9999999999999");
        }


        if ($this->getSortOrder() == "rating") {
            if ($this->getDirection() == "desc") {
                $collection->getSelect()->order('rating DESC');
            } else {
                $collection->getSelect()->order('rating ASC');
            }
        } else {
            if ($this->getDirection() == "desc") {
                $collection->getSelect()->order('price DESC');
            } else {
                $collection->getSelect()->order('price ASC');
            }
        }
        return $collection;
    }

    /**
     * [getProduct description]
     * @return [type] [description]
     */
    public function getProduct()
    {
        return $this->_coreRegistry->registry('product');
    }

    /**
     * [getDirection description]
     * @return [type] [description]
     */
    public function getDirection()
    {
        $dir = $this->_request->getParam("list_dir");
        if ($dir != "desc") {
            $dir = "asc";
        }
        return $dir;
    }

    /**
     * [getSortOrder description]
     * @return [type] [description]
     */
    public function getSortOrder()
    {
        $order = $this->_request->getParam("list_order");
        if ($order != "rating") {
            $order = "price";
        }
        return $order;
    }

    /**
     * [getDefaultUrl description]
     * @return [type] [description]
     */
    public function getDefaultUrl()
    {
        $currentUrl = $this->getUrl('*/*/*', ['_current' => true, '_use_rewrite' => true]);
        list($url) = explode("?", $currentUrl);
        return $url;
    }
}
