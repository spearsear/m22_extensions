<?php

namespace Meetanshi\Mobilelogin\Controller\Otp;

use Magento\Customer\Controller\AbstractAccount;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Meetanshi\Mobilelogin\Helper\Data;

class Resetpassword extends AbstractAccount
{
    private $helper;
    protected $mobileloginFactory;
    private $jsonFactory;

    public function __construct(
        Context $context,
        Data $helperData,
        JsonFactory $jsonFactory
    ) {
    
        $this->helper = $helperData;
        $this->jsonFactory = $jsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->jsonFactory->create();
        try {
            $param = $this->getRequest()->getParams();
            $response = [
                'succeess' => "true",
                'errormsg' => "Some Thing went wrong",
                'successmesg' => "",
                'customurl' => ''
            ];
            $customer = $this->helper->getCustomerCollectionMobile($param['mobilenumber']);
            if (count($customer)) {
                $customer->setMobileNumber($param['mobilenumber']);
                $customer->setRpToken($customer->getRpToken());
                $customer->setPassword($param['password']);
                $customer->save();
                $response['customurl'] = $this->url->getUrl('customer/account/login');
                $this->messageManager->addSuccessMessage(__('Password Change Successfully'));
            } else {
                $this->messageManager->addErrorMessage(__('Password Change Error'));
                $response['succeess'] = "false";
            }

            $result->setData($response);
            return $result;
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('Password Change Error'));
            return $result;
        }
    }
}
