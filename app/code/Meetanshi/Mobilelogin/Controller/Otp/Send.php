<?php

namespace Meetanshi\Mobilelogin\Controller\Otp;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Meetanshi\Mobilelogin\Helper\Data;

class Send extends Action
{
    private $helper;

    public function __construct(Context $context, Data $helper
    )
    {
        $this->helper = $helper;
        parent::__construct($context);
    }

    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->renderLayout();
        return $this->helper->otpSave($this->getRequest()->getParams());
    }
}
