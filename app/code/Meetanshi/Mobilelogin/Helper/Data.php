<?php

namespace Meetanshi\Mobilelogin\Helper;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory as CustomerCollection;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Meetanshi\Mobilelogin\Model\ResourceModel\Mobilelogin\CollectionFactory;
use Twilio\Rest\ClientFactory as TwilioClientFactory;
use Qcloud\Sms\SmsSingleSender;
use Qcloud\Sms\SmsMultiSender;

class Data extends AbstractHelper
{

    const MOBILELOGIN_ENABLED = 'mobilelogin/general/enabled';
    const OTP_LENGTH = 'mobilelogin/general/otplength';
    const OTP_TYPE = 'mobilelogin/general/otptype';
    const APIPROVIDER = 'mobilelogin/apisetting/apiprovider';
    const SENDER = 'mobilelogin/apisetting/senderid';
    const MESSAGETYPE = 'mobilelogin/apisetting/messagetype';
    const APIURL = 'mobilelogin/apisetting/apiurl';
    const APIKEY = 'mobilelogin/apisetting/apikey';
    const SID = 'mobilelogin/apisetting/sid';
    const TOKEN = 'mobilelogin/apisetting/token';
    const FROMMOBILENUMBER = 'mobilelogin/apisetting/frommobilenumber';
    const COUNTRYCODE = 'mobilelogin/apisetting/countrycode';
    const LOGINENABLED = 'mobilelogin/otpsend/loginenabled';
    const REGISTERENABLED = 'mobilelogin/otpsend/registerenabled';
    const FORGOTENABLED = 'mobilelogin/otpsend/forgotenabled';
    const UPDATEENABLED = 'mobilelogin/otpsend/updateenabled';
    const SMS_REGISTER = 'mobilelogin/otpsend/registrationmessage';
    const SMS_FORGOT = 'mobilelogin/otpsend/forgotmessage';
    const SMS_LOGIN = 'mobilelogin/otpsend/loginmessage';
    const SMS_UPDATE = 'mobilelogin/otpsend/updatemessage';

    const APP_ID = 'mobilelogin/apisetting/app_id';
    const APP_KEY = 'mobilelogin/apisetting/app_key';
    const TEMPLATE_ID = 'mobilelogin/apisetting/template_id';
    const SMS_SIGN = 'mobilelogin/apisetting/sms_sign';

    private $pageFactory;
    protected $mobileloginFactory;
    private $jsonFactory;
    protected $customerFactory;
    private $customerSession;
    private $resultRedirect;
    private $storeManagerInterface;
    private $customerRepository;
    private $customer;
    private $url;
    private $messageManager;
    private $directory;
    private $twilioClientFactory;
    private $httpContext;

    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        CollectionFactory $collectionFactory,
        CustomerCollection $customerFactory,
        Session $customerSession,
        ResultFactory $result,
        StoreManagerInterface $storeManager,
        CustomerRepositoryInterface $customerRepository,
        Customer $customer,
        UrlInterface $url,
        ManagerInterface $messageManager,
        DirectoryList $directoryList,
        TwilioClientFactory $twilioClientFactory,
        HttpContext $httpContext
    ) {
    
        $this->jsonFactory = $jsonFactory;
        $this->mobileloginFactory = $collectionFactory;
        $this->customerFactory = $customerFactory;
        $this->customerSession = $customerSession;
        $this->resultRedirect = $result;
        $this->storeManagerInterface = $storeManager;
        $this->customerRepository = $customerRepository;
        $this->customer = $customer;
        $this->url = $url;
        $this->messageManager = $messageManager;
        $this->directory = $directoryList;
        $this->twilioClientFactory = $twilioClientFactory;
        $this->httpContext = $httpContext;
        parent::__construct($context);
    }

    public function getConfig($value)
    {
        return $this->scopeConfig->getValue($value, ScopeInterface::SCOPE_STORE);
    }

    public function customerLoggedIn()
    {
        return (bool)$this->httpContext->getValue(\Magento\Customer\Model\Context::CONTEXT_AUTH);
    }

    public function isMobileloginEnabled()
    {
        return $this->getConfig(self::MOBILELOGIN_ENABLED);
    }

    public function getApiprovider()
    {
        return $this->getConfig(self::APIPROVIDER);
    }

    public function getSid()
    {
        return $this->getConfig(self::SID);
    }

    public function getToken()
    {
        return $this->getConfig(self::TOKEN);
    }

    public function getAdminmobile()
    {
        return $this->getConfig(self::FROMMOBILENUMBER);
    }

    public function getCountrycode()
    {
        return $this->getConfig(self::COUNTRYCODE);
    }

    public function isAllowtologinPage()
    {
        if ($this->isEnable()) {
            return $this->getConfig(self::LOGINENABLED);
        } else {
            return false;
        }
    }

    public function isAllowtoForgotPage()
    {
        if ($this->isEnable()) {
            return $this->getConfig(self::FORGOTENABLED);
        } else {
            return false;
        }
    }

    public function isAllowtoRegisterPage()
    {
        if ($this->isEnable()) {
            return $this->getConfig(self::REGISTERENABLED);
        } else {
            return false;
        }
    }

    public function isAllowtoUpdatePage()
    {
        if ($this->isEnable()) {
            return $this->getConfig(self::UPDATEENABLED);
        } else {
            return flase;
        }
    }

    public function getCustomerMobile()
    {
        return $this->customerSession->getCustomer()->getMobileNumber();
    }

    public function getCustomerCollection()
    {
        return $this->customerFactory->create();
    }

    public function loadCustomerById($id)
    {
        $customer = $this->customerRepository->getById($id);
        return $customer;
    }

    public function getStoreName()
    {
        return $this->getConfig('general/store_information/name');
    }

    public function getWebsiteId()
    {
        return $this->storeManagerInterface->getStore()->getWebsiteId();
    }

    public function getCustomerCollectionMobile($mobile)
    {
        try {
            $customer = $this->customerFactory->create()
                ->addAttributeToSelect("*")
                ->addAttributeToFilter("mobile_number", ["eq" => $mobile])
                ->getFirstItem();
            return $customer;
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage("Error" . $e->getMessage());
        }
    }

    public function isEnable()
    {
        return $this->scopeConfig->getValue(self::MOBILELOGIN_ENABLED, ScopeInterface::SCOPE_STORE);
    }

    public function otpVerify($data)
    {
        $otptype = $data['otptype'];
        $mobilenumber = $data['mobilenumber'];
        $verifyCode = $data['otpcode'];
        $oldMobilenumer = "";
        if (isset($data['oldmobile'])) {
            $oldMobilenumer = $data['oldmobile'];
        }

        $response = [
            'succeess' => "false",
            'errormsg' => "Invalid OTP",
            'successmesg' => "",
            'customurl' => ""
        ];
        $resultPage = $this->mobileloginFactory->create()
            ->addFieldToFilter('mobilenumber', ['eq' => $mobilenumber])
            ->getFirstItem();
        if (count($resultPage)) {
            if ($otptype == "register") {
                if ($resultPage->getRegisterOtp() == $verifyCode) {
                    $resultPage->setRegisterVerify(1);
                    $resultPage->save();
                    $response['succeess'] = "true";
                    $response['successmesg'] = "Otp Verified Successfully";
                }
            }
            if ($otptype == "forgot") {
                if ($resultPage->getForgotOtp() == $verifyCode) {
                    $resultPage->setForgotVerify(1);
                    $resultPage->save();
                    $response['succeess'] = "true";
                    $response['successmesg'] = "Otp Verified Successfully";
                }
            }
            if ($otptype == "login") {
                if ($resultPage->getLoginOtp() == $verifyCode) {
                    $resultPage->setLoginVerify(1);
                    $resultPage->save();
                    $this->customer->setWebsiteId($this->getWebsiteId());
                    $customerData = $this->customer->loadByEmail($this->getCustomerCollectionMobile($mobilenumber)->getEmail());
                    $this->customerSession->setCustomerAsLoggedIn($customerData);
                    $response['customurl'] = $this->url->getUrl('customer/account/');
                    $response['successmesg'] = "Otp Verified Successfully";
                    $response['succeess'] = "true";
                }
            }
            if ($otptype == "update") {
                if ($resultPage->getUpdateOtp() == $verifyCode) {
                    $resultPage->setUpdateVerify(1);
                    $resultPage->setMobilenumber($mobilenumber);
                    $resultPage->save();
                    $customer = $this->loadCustomerById($this->customerSession->getCustomer()->getEntityId());
                    $customer->setCustomAttribute('mobile_number', $mobilenumber);
                    $this->customerRepository->save($customer);
                    if ($oldMobilenumer != "") {
                        $this->messageManager->addSuccessMessage("Mobile Updated from " . $oldMobilenumer . " to " . $mobilenumber . " Succeessfully");
                    } else {
                        $this->messageManager->addSuccessMessage("Mobile Number Updated Succeessfully");
                    }
                    $response['succeess'] = "true";
                    $response['successmesg'] = "Otp Verified Successfully";
                }
            }
        }
        $result = $this->jsonFactory->create();
        $result->setData($response);
        return $result;
    }

    public function getStoreUrl()
    {
        $this->storeManagerInterface->getStore()->getBaseUrl();
    }

    public function otpSave($data)
    {
        try {
            if (isset($data['otptype']) and isset($data['mobilenumber'])) {
                $otptype = $data['otptype'];
                $mobilenumber = $data['mobilenumber'];
                $countryCode = $data['countrycode'];
                $resendotp = $data['resendotp'];
                $oldMobilenumber = $data['oldmobile'];
                $otpcode = $this->generateOtpCode();
                $response = [
                    'succeess' => "true",
                    'errormsg' => "",
                    'successmsg' => ""
                ];
                if ($otptype == "register") {
                    if (count($this->getCustomerCollectionMobile($mobilenumber)->getData())) {
                        $response['succeess'] = 'false';
                        $response['errormsg'] = 'Mobile number is already register';
                    } else {
                        $resultPage = $this->mobileloginFactory->create()
                            ->addFieldToFilter('mobilenumber', ['eq' => $mobilenumber])
                            ->getFirstItem();
                        if (!count($resultPage->getData())) {
                            $resultPage->setMobilenumber($mobilenumber);
                        }
                        $resultPage->setRegisterOtp($otpcode)
                            ->setRegisterVerify(0)
                            ->save();
                        $response['successmsg'] = "New OTP Sent on your Mobile " . $mobilenumber;
                        $message = $this->getMessageText($otpcode, $otptype);
                        if ($this->curlApi($mobilenumber, $message,$countryCode)) {
                            $response['successmsg'] = "OTP Sent on your Mobile " . $mobilenumber;
                        } else {
                            $response['succeess'] = 'false';
                            $response['errormsg'] = 'Message Send Failed';
                        }
                    }
                } elseif ($otptype == "forgot") {
                    if (!count($this->getCustomerCollectionMobile($mobilenumber)->getData())) {
                        $response['succeess'] = 'false';
                        $response['errormsg'] = 'Mobile number is not register';
                    } else {
                        $resultPage = $this->mobileloginFactory->create()
                            ->addFieldToFilter('mobilenumber', ['eq' => $mobilenumber])
                            ->getFirstItem();

                        if (count($resultPage->getData())) {
                            $resultPage->setForgotOtp($otpcode)
                                ->setForgotVerify(0)
                                ->save();
                        } else {
                            $response['succeess'] = 'false';
                            $response['errormsg'] = 'Mobile number not found';
                        }
                        $message = $this->getMessageText($otpcode, $otptype);
                        if ($this->curlApi($mobilenumber, $message,$countryCode)) {
                            $response['successmsg'] = "OTP Sent on your Mobile " . $mobilenumber;
                        } else {
                            $response['succeess'] = 'false';
                            $response['errormsg'] = 'Message Send Failed';
                        }
                    }
                } elseif ($otptype == "update") {
                    if (count($this->getCustomerCollectionMobile($mobilenumber)->getData())) {
                        $response['succeess'] = 'false';
                        $response['errormsg'] = 'Mobile number is already register';
                    } else {
                        $resultPage = $this->mobileloginFactory->create()
                            ->addFieldToFilter('mobilenumber', ['eq' => $mobilenumber])
                            ->getFirstItem();

                        if (count($resultPage->getData())) {
                            $resultPage->setUpdateOtp($otpcode)
                                ->setUpdateVerify(0)
                                ->save();
                        } else {
                            $resultPage->setMobilenumber($mobilenumber)
                                ->setUpdateOtp($otpcode)
                                ->setUpdateVerify(0)
                                ->save();
                        }
                        $response['successmsg'] = "OTP Sent on your Mobile " . $mobilenumber;
                        $message = $this->getMessageText($otpcode, $otptype);
                        if ($this->curlApi($mobilenumber, $message,$countryCode)) {
                            $response['successmsg'] = "OTP Sent on your Mobile " . $mobilenumber;
                        } else {
                            $response['succeess'] = 'false';
                            $response['errormsg'] = 'Message Send Failed';
                        }
                    }
                } elseif ($otptype == "login") {
                    if (!count($this->getCustomerCollectionMobile($mobilenumber)->getData())) {
                        $response['succeess'] = 'false';
                        $response['errormsg'] = 'Mobile number is not register';
                    } else {
                        $resultPage = $this->mobileloginFactory->create()
                            ->addFieldToFilter('mobilenumber', ['eq' => $mobilenumber])
                            ->getFirstItem();

                        if (count($resultPage->getData())) {
                            $resultPage->setLoginOtp($otpcode)
                                ->setLoginVerify(0)
                                ->save();
                        } else {
                            $resultPage->setMobilenumber($mobilenumber)
                                ->setLoginOtp($otpcode)
                                ->setLoginVerify(0)
                                ->save();
                        }
                        $response['successmsg'] = "OTP Sent on your Mobile " . $mobilenumber;
                        $message = $this->getMessageText($otpcode, $otptype);
                        if ($this->curlApi($mobilenumber, $message,$countryCode)) {
                            $response['successmsg'] = "OTP Sent on your Mobile " . $mobilenumber;
                        } else {
                            $response['succeess'] = 'false';
                            $response['errormsg'] = 'Message Send Failed';
                        }
                    }
                }
                $result = $this->jsonFactory->create();
                $result->setData($response);
                return $result;
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage("Otp Save Error" . $e->getMessage());
        }
    }

    public function getOtpLength()
    {
        $length = $this->getConfig(self::OTP_LENGTH);
        return $length;
    }

    public function generateOtpCode()
    {
        $length = $this->getOtpLength();
        $otptype = $this->getConfig(self::OTP_TYPE);
        if ($otptype == 1) {
            $randomString = substr(str_shuffle("0123456789"), 0, $length);
        } elseif ($otptype == 2) {
            $randomString = substr(str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
        } else {
            $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
        }
        return $randomString;
    }

    public function getMessageText($otpcode, $otptype)
    {
        try {
            $storename = $this->getStoreName();
            $storeUrl = $this->storeManagerInterface->getStore()->getBaseUrl();
            $message = "";
            if ($otptype == "register") {
                $message = $this->getConfig(self::SMS_REGISTER);
            } elseif ($otptype == "forgot") {
                $message = $this->getConfig(self::SMS_FORGOT);
            } elseif ($otptype == "login") {
                $message = $this->getConfig(self::SMS_LOGIN);
            } elseif ($otptype == "update") {
                $message = $this->getConfig(self::SMS_UPDATE);
            }

            $replaceArray = [$otpcode, $storename, $storeUrl];
            $originalArray = ['{{otp_code}}', '{{shop_name}}', '{{shop_url}}'];
            $newMessage = str_replace($originalArray, $replaceArray, $message);
            return $newMessage;
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }
    }

    public function curlApi($mobilenumber, $message,$countryCode)
    {
        try {
            if ($this->isEnable()) {
                if ($this->getApiprovider() == "msg91") {
                    $msg = urlencode($message);
                    $apikey = $this->getConfig(self::APIKEY);
                    $senderid = $this->getConfig(self::SENDER);
                    $url = $this->getConfig(self::APIURL);
                    $msgtype = $this->getConfig(self::MESSAGETYPE);

                    $postUrl = $url . "?sender=" . $senderid . "&route=" . $msgtype . "&mobiles=" . $mobilenumber . "&authkey=" . $apikey . "&message=" . $msg . "";
                    $curl = curl_init();
                    curl_setopt_array(
                        $curl,
                        [
                            CURLOPT_URL => $postUrl,
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => "",
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 30,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => "GET",
                            CURLOPT_SSL_VERIFYHOST => 0,
                            CURLOPT_SSL_VERIFYPEER => 0,
                        ]
                    );
                    $response = curl_exec($curl);
                    $err = curl_error($curl);
                    curl_close($curl);
                    if ($err) {
                        return "cURL Error #:" . $err;
                    } else {
                        if ($response) {
                            return true;
                        }
                    }
                } elseif ($this->getApiprovider() == "textlocal") {
                    $url = $this->getConfig(self::APIURL);
                    $apiKey = urlencode($this->getConfig(self::APIKEY));
                    $numbers = [$mobilenumber];
                    $sender = urlencode($this->getConfig(self::SENDER));
                    $message = rawurlencode($message);
                    $numbers = implode(',', $numbers);
                    $data = ['apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message];

                    $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $response = curl_exec($ch);
                    curl_close($ch);

                    $responseArray = json_decode($response);
                    if ($responseArray['status'] == "success") {
                        return true;
                    } else {
                        return false;
                    }
                } elseif ($this->getApiprovider() == "twilio") {
                    $sid = $this->getConfig(self::SID);
                    $token = $this->getConfig(self::TOKEN);
                    $fromMobile = $this->getConfig(self::FROMMOBILENUMBER);
                    $twilio = $this->twilioClientFactory->create([
                        'username' => $sid,
                        'password' => $token
                    ]);
                    $countryCode = $this->getConfig(self::COUNTRYCODE);
                    $message = $twilio->messages
                        ->create(
                            $countryCode . $mobilenumber,
                            [
                                "body" => $message,
                                "from" => $fromMobile
                            ]
                        );
                    if ($message->sid) {
                        return true;
                    } else {
                        return false;
                    }
                } elseif ($this->getApiprovider() == "cloudsms"){

                    require_once('qcloudsms_php/src/index.php');

                    $appId = $this->getConfig(self::APP_ID);
                    $appKey = $this->getConfig(self::APP_KEY);
                    $templateId = $this->getConfig(self::TEMPLATE_ID);
                    $smsSign = $this->getConfig(self::SMS_SIGN);

                    $mobilenumber = str_replace('+'.$countryCode,'',$mobilenumber);

                    if ($smsSign == '' || $smsSign == null){
                        $smsSign = "腾讯云";
                    }

                    try {
                        $sender = new SmsSingleSender($appId, $appKey);
                        $params = ["$message"];
                        $result = $sender->sendWithParam("$countryCode", $mobilenumber, $templateId,
                            $params, $smsSign, "", "");
                        $rsp = json_decode($result);

                        if ($rsp->result == 0 && $rsp->sid) {
                            return true;
                        } else {
                            return false;
                        }
                    } catch(\Exception $e) {
                        $this->messageManager->addErrorMessage($e->getMessage());
                        return false;
                    }
                }
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage("Message Send Error".$e->getMessage());
        }
    }
}
