<?php

namespace Meetanshi\Mobilelogin\Block;

use Magento\Framework\View\Element\Template;
use Meetanshi\Mobilelogin\Helper\Data;

class Update extends Template
{
    private $helper;

    public function __construct(
        Template\Context $context,
        Data $helper
    ) {
    
        $this->helper = $helper;
        parent::__construct($context);
    }

    public function getCustomerMobileNumber()
    {
        return $this->helper->getCustomerMobile();
    }
}
