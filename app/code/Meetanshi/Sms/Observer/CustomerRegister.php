<?php

namespace Meetanshi\Sms\Observer;

use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;
use Meetanshi\Sms\Helper\Data;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;

class CustomerRegister implements ObserverInterface
{
    const SMS_ADMIN_MOBILE = 'sms/general/mobilenumber';
    const SMS_REGISTER_ENABLE = 'sms/register/enabled';
    const SMS_REGISTER_SMSTEXT = 'sms/register/smstext';

    protected $logger;
    protected $helper;
    protected $scopeConfig;
    protected $storeManager ;

    public function __construct(
        LoggerInterface $logger,
        ScopeConfigInterface $scopeConfig,
        Data $data,
        StoreManagerInterface $storeManager
    ) {
    
        $this->logger = $logger;
        $this->helper = $data;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            if ($this->helper->smsEnable()) {
                $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
                $enableInContact = $this->scopeConfig->getValue(self::SMS_REGISTER_ENABLE, $storeScope);
                if ($enableInContact) {
                    $adminMobile = $this->scopeConfig->getValue(self::SMS_ADMIN_MOBILE, $storeScope);
                    $contactText = $this->scopeConfig->getValue(self::SMS_REGISTER_SMSTEXT, $storeScope);

                    $storeurl = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_LINK, true);
                    $storename = $this->storeManager->getStore()->getName();

                    $customer = $observer->getData('customer');
                    $fname = $customer->getFirstName();
                    $lname = $customer->getLastname();
                    $email = $customer->getEmail();

                    $codes = ['{{shop_name}}', '{{shop_url}}', '{{first_name}}', '{{last_name}}', '{{email}}'];
                    $accurate = [$storename, $storeurl, $fname, $lname,$email];

                    $finalContactText = str_replace($codes, $accurate, $contactText);

                    $apiProvider = $this->helper->getApi();
                    if ($apiProvider == 'cloudsms'){
                        $countryCode = str_replace('+','',$this->helper->adminCountryCode());
                        $adminMobile = str_replace($countryCode,'',$adminMobile);
                        $this->helper->apiCall($finalContactText, $adminMobile, $countryCode);
                    }else {
                        $this->helper->apiCall($finalContactText, $adminMobile);
                    }
                }
            }
            return true;
        } catch (\Exception $e) {
            $this->logger->info($e->getMessage());
        }
    }
}
