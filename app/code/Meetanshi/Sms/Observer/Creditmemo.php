<?php

namespace Meetanshi\Sms\Observer;

use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;
use Meetanshi\Sms\Helper\Data;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Pricing\Helper\Data as PriceHelper;

class Creditmemo implements ObserverInterface
{
    const SMS_ADMIN_MOBILE = 'sms/general/mobilenumber';

    const SMS_CREDITMEMO_ENABLE = 'sms/creditmemo/enabled';
    const SMS_CREDITMEMO_MSGTOADMIN = 'sms/creditmemo/msgtoadmin';
    const SMS_CREDITMEMO_SMSTEXT = 'sms/creditmemo/smstext';
    const SMS_CREDITMEMO_SMSTEXTADMIN = 'sms/creditmemo/smstextadmin';

    protected $logger;
    protected $helper;
    protected $scopeConfig;
    protected $storeManager ;
    protected $priceHelper ;

    public function __construct(
        LoggerInterface $logger,
        ScopeConfigInterface $scopeConfig,
        Data $data,
        StoreManagerInterface $storeManager,
        PriceHelper $priceHelper
    ) {
    
        $this->logger = $logger;
        $this->helper = $data;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->priceHelper = $priceHelper;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            if ($this->helper->smsEnable()) {
                $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
                $enableInOrderPlace = $this->scopeConfig->getValue(self::SMS_CREDITMEMO_ENABLE, $storeScope);
                $enableForadmin = $this->scopeConfig->getValue(self::SMS_CREDITMEMO_MSGTOADMIN, $storeScope);
                $apiProvider = $this->helper->getApi();

                if ($enableInOrderPlace) {
                    $adminMobile = $this->scopeConfig->getValue(self::SMS_ADMIN_MOBILE, $storeScope);
                    $msgText = $this->scopeConfig->getValue(self::SMS_CREDITMEMO_SMSTEXT, $storeScope);

                    $storeurl = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_LINK, true);
                    $storename = $this->storeManager->getStore()->getName();

                    $creditMemo = $observer->getEvent()->getCreditmemo();
                    $order = $creditMemo->getOrder();

                    $mobilenumber = $order->getBillingAddress()->getTelephone();
                    $countryId   = $order->getBillingAddress()->getCountryId();
                    $countryCode = $this->helper->getCountryCode($countryId);
                    $mobilenumber= $countryCode.$mobilenumber;

                    $orderTotal =$this->priceHelper->currency($order->getGrandTotal(), true, false);

                    $codes = ['{{shop_name}}', '{{shop_url}}', '{{first_name}}', '{{last_name}}', '{{creditmemo_total}}', '{{order_id}}'];
                    $accurate = [$storename, $storeurl,$order->getBillingAddress()->getFirstname(),$order->getBillingAddress()->getLastname(),$orderTotal ,'#'.$order->getIncrementId()];

                    $finalContactText = str_replace($codes, $accurate, $msgText);

                    if ($apiProvider == 'cloudsms'){
                        $mobile = str_replace($countryCode,'',$mobilenumber);
                        $this->helper->apiCall($finalContactText, $mobile, $countryCode);
                    }else {
                        $this->helper->apiCall($finalContactText, $mobilenumber);
                    }

                    if ($enableForadmin) {
                        $msgText = $this->scopeConfig->getValue(self::SMS_CREDITMEMO_SMSTEXTADMIN, $storeScope);
                        $codes = ['{{shop_name}}', '{{shop_url}}', '{{first_name}}', '{{last_name}}', '{{creditmemo_total}}', '{{order_id}}'];
                        $accurate = [$storename, $storeurl,$order->getBillingAddress()->getFirstname(),$order->getBillingAddress()->getLastname(),$orderTotal ,'#'.$order->getIncrementId()];

                        $finalContactText = str_replace($codes, $accurate, $msgText);

                        if ($apiProvider == 'cloudsms'){
                            $countryCode = str_replace('+','',$this->helper->adminCountryCode());
                            $adminMobile = str_replace($countryCode,'',$adminMobile);
                            $this->helper->apiCall($finalContactText, $adminMobile, $countryCode);
                        }else {
                            $this->helper->apiCall($finalContactText, $adminMobile);
                        }
                    }
                }
            }
            return true;
        } catch (\Exception $e) {
            $this->logger->info($e->getMessage());
        }
    }
}
