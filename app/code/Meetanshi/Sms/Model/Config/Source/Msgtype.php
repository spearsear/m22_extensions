<?php

namespace Meetanshi\Sms\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class Msgtype implements ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => '4', 'label' => __('Transactional')],
            ['value' => '1', 'label' => __('Promotional')]
        ];
    }
}
