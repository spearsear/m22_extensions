<?php

namespace Meetanshi\Sms\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class Apiprovider implements ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => 'msg', 'label' => __('MSG 91')],
            ['value' => 'textlocal', 'label' => __('Text Local')],
            ['value' => 'twilio', 'label' => __('Twilio')],
            ['value' => 'cloudsms', 'label' => __('Tencent Cloud Sms')]
        ];
    }
}
