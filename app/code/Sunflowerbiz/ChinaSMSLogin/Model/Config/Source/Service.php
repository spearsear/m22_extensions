<?php

namespace Sunflowerbiz\ChinaSMSLogin\Model\Config\Source;

class Service implements \Magento\Framework\Option\ArrayInterface {

    public function getAllOptions() {
        
          	$path=str_replace('Model'.DIRECTORY_SEPARATOR.'Config'.DIRECTORY_SEPARATOR.'Source','Controller'.DIRECTORY_SEPARATOR.'service'.DIRECTORY_SEPARATOR.'',dirname(__FILE__));
			if($handle = opendir($path)){
			  while (false !== ($file = readdir($handle))){
			  	if(is_dir($path.DIRECTORY_SEPARATOR.$file) && $file!='.' && $file!='..'){
					$filename=str_replace('.php','',$file);
					 $options[] = array(
						'value' => $filename,
						'label' => $filename,
					);
				}
			  }
			  closedir($handle);
			}
          
            $this->_options = $options;
       
        return $this->_options;
    }

    public function toOptionArray() {
        return $this->getAllOptions();
    }

}