<?php

namespace Sunflowerbiz\ChinaSMSLogin\Controller\Process;

class SMSlogin extends \Magento\Framework\App\Action\Action 
{
	protected $storeManager;
	protected $customerFactory;
	protected $customerRepository;
	protected $searchCriteriaBuilder;
	protected $filterBuilder;
	protected $ustomerSession;
	protected $messageManager;
    public function __construct(
        \Magento\Framework\App\Action\Context $context
    ) {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$this->storeManager     =  $objectManager->create('\Magento\Store\Model\StoreManagerInterface');
        $this->customerFactory  =  $objectManager->create('\Magento\Customer\Model\CustomerFactory');
        $this->customerRepository  = $objectManager->create('\Magento\Customer\Api\CustomerRepositoryInterface');
        $this->searchCriteriaBuilder  =$objectManager->create('\Magento\Framework\Api\SearchCriteriaBuilder');
        $this->filterBuilder  = $objectManager->create('\Magento\Framework\Api\FilterBuilder');
		$this->customerSession = $objectManager->create('Magento\Customer\Model\Session');
		$this->messageManager = $objectManager->create('\Magento\Framework\Message\ManagerInterface');
        parent::__construct($context);
    }

  
    /**
     * Set redirect
     */
    public function execute()
    {
	
	
	
	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
	$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	$core_write = $resource->getConnection();
	$urlInterface = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\UrlInterface');
     $baseurl = $urlInterface->getBaseUrl().'chinaSMSlogin/process/SMSlogin';
	 
	$return = $this->getRequest()->getParam('return',false);	
	
	if($return!="")
	$this->customerSession->setData('chinaSMSlogin_return',$return);
	
	$redirectpage= $this->customerSession->getData('chinaSMSlogin_return');
	$phone =$this->getRequest()->getParam('phone');
			$vcode =$this->getRequest()->getParam('vcode');
			
			$session_phone=$this->customerSession->getData('chinaSMSlogin_SmsPhone');
			$session_code=$this->customerSession->getData('chinaSMSlogin_SmsCode');
			$success=false;
			if($phone==$session_phone && $vcode==$session_code){
				$success=true;
				$this->check_account($phone);
				 $result = array(
					'status' => 'success',
					'msg' => 'Successfully',
				);
			}else{
		    $result['status'] = 'invalid';
            $result['message'] = __('Invalid Verification code');//.$phone.'-'.$vcode.'*'.$session_phone.'-'.$session_code
			}
			 
            $this->getResponse()->setHeader('Content-type', 'application/json');
			 $this->getResponse()->setHeader('Cache-Control', 'no-cache, must-revalidate');
            $this->getResponse()->setBody($objectManager->create('\Magento\Framework\Json\Helper\Data')->jsonEncode($result));
	
	return ;
 
    }
	public function check_account($phone){
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		  $filters = [];
  		$filters[] = $this->filterBuilder
            ->setField('mobile_no')
            ->setConditionType('like')
            ->setValue($phone. '%')//
            ->create();
			$this->searchCriteriaBuilder->addFilters($filters);
			$searchCriteria = $this->searchCriteriaBuilder->create();
			
			$searchResults = $this->customerRepository->getList($searchCriteria);
			$uidExist=count($searchResults->getItems());
			
			if ($uidExist) {
			
		 foreach ($searchResults->getItems() as $customer) {
		 	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			 $CustomerModel = $objectManager->create('Magento\Customer\Model\Customer');
			  $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();
			  $CustomerModel->setWebsiteId($websiteId);
			 $CustomerModel->loadByEmail($customer->getEmail());
			 $this->messageManager->addSuccess(__('We create an account for you, Please change your information!'));
           $this->customerSession->setCustomerAsLoggedIn($CustomerModel);
		   return;
		 }
		
			
        } else {
			
			$default_firstname =  $objectManager->create('Sunflowerbiz\ChinaSMSLogin\Helper\Data')->getConfig('se_chinasmslogin/chinasmslogin/default_firstname');
			$default_lastname =  $objectManager->create('Sunflowerbiz\ChinaSMSLogin\Helper\Data')->getConfig('se_chinasmslogin/chinasmslogin/default_lastname');
			$default_email =  $objectManager->create('Sunflowerbiz\ChinaSMSLogin\Helper\Data')->getConfig('se_chinasmslogin/chinasmslogin/default_email');
			
		 $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();
        // Instantiate object (this is the most important part)
        $customer   = $this->customerFactory->create();
        $customer->setWebsiteId($websiteId);

        // Preparing data for new customer
        $customer->setEmail( str_replace('{phone}',$phone,$default_email)); 
        $customer->setFirstname( $default_firstname);
        $customer->setLastname($default_lastname);
        $customer->setPassword("SmsLoginTmpPassword");
        $customer->setMobileNo($phone);

        // Save data
        $customer->save();
		 $this->messageManager->addSuccess(__('We create an account for you, Please change your information!'));
		$this->customerSession->setCustomerAsLoggedIn($customer);
		
     	 //$customer->sendNewAccountEmail();

           
        }
	
	}
	function redirect($url){
		$resultRedirect = $this->resultRedirectFactory->create();
		$resultRedirect->setPath($url);
		return $resultRedirect;
	}
	

   
}