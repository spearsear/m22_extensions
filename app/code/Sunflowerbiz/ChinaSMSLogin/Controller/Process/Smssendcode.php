<?php

namespace Sunflowerbiz\ChinaSMSLogin\Controller\Process;

class Smssendcode extends \Magento\Framework\App\Action\Action 
{
	protected $storeManager;
	protected $customerFactory;
	protected $customerRepository;
	protected $searchCriteriaBuilder;
	protected $filterBuilder;
	protected $ustomerSession;
	protected $messageManager;
    public function __construct(
        \Magento\Framework\App\Action\Context $context
    ) {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$this->storeManager     =  $objectManager->create('\Magento\Store\Model\StoreManagerInterface');
        $this->customerFactory  =  $objectManager->create('\Magento\Customer\Model\CustomerFactory');
        $this->customerRepository  = $objectManager->create('\Magento\Customer\Api\CustomerRepositoryInterface');
        $this->searchCriteriaBuilder  =$objectManager->create('\Magento\Framework\Api\SearchCriteriaBuilder');
        $this->filterBuilder  = $objectManager->create('\Magento\Framework\Api\FilterBuilder');
		$this->customerSession = $objectManager->create('Magento\Customer\Model\Session');
		$this->messageManager = $objectManager->create('\Magento\Framework\Message\ManagerInterface');
        parent::__construct($context);
    }

  
    /**
     * Set redirect
     */
    public function execute()
    {
	
	
	
	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
	$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	$core_write = $resource->getConnection();
	$urlInterface = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\UrlInterface');
     $baseurl = $urlInterface->getBaseUrl().'chinaSMSlogin/process/SMSlogin';
	 

     $currenturl = $urlInterface->getCurrentUrl();
			
			$appid =  $objectManager->create('Sunflowerbiz\ChinaSMSLogin\Helper\Data')->getConfig('se_chinasmslogin/chinasmslogin/app_id');
			$appkey =  $objectManager->create('Sunflowerbiz\ChinaSMSLogin\Helper\Data')->getConfig('se_chinasmslogin/chinasmslogin/app_key');
			$sms_sign =  $objectManager->create('Sunflowerbiz\ChinaSMSLogin\Helper\Data')->getConfig('se_chinasmslogin/chinasmslogin/sms_sign');
			$login_template_id =  $objectManager->create('Sunflowerbiz\ChinaSMSLogin\Helper\Data')->getConfig('se_chinasmslogin/chinasmslogin/login_template_id');
			$login_template_params =  $objectManager->create('Sunflowerbiz\ChinaSMSLogin\Helper\Data')->getConfig('se_chinasmslogin/chinasmslogin/login_template_params');
			$wait_time =  $objectManager->create('Sunflowerbiz\ChinaSMSLogin\Helper\Data')->getConfig('se_chinasmslogin/chinasmslogin/wait_time');
			$service =  $objectManager->create('Sunflowerbiz\ChinaSMSLogin\Helper\Data')->getConfig('se_chinasmslogin/chinasmslogin/service');
			$active_log_stock_update =  $objectManager->create('Sunflowerbiz\ChinaSMSLogin\Helper\Data')->getConfig('se_chinasmslogin/chinasmslogin/enable_log');
			
			$phone =$this->getRequest()->getParam('phone');
			$countryno = $this->getRequest()->getParam('countryno');
			$vcode =$this->getRequest()->getParam('vcode');
			$code = rand(1000, 9999);
			
		 	$str_params=array('{code}','{wait_time}');
		 	$real_params=array($code,$wait_time);
			
			$login_template_params=str_replace(($str_params),($real_params),$login_template_params);
			
			
			$params=explode(',',$login_template_params);	
			
			$service=trim(strtolower($service));
			include_once str_replace('Process','',dirname(__FILE__)).'/service/'.$service.'/CloudSender.php';
			$parameters = ['params' => [$appid, $appkey]];
			$ssender= $objectManager->create('Sunflowerbiz\ChinaSMSLogin\Controller\service\\'.$service.'\CloudSender');
			$ssender->init($appid, $appkey);
		 	 $smsresult = $ssender->sendWithParam($countryno, $phone, $login_template_id, $params, $sms_sign, "", "");
			 $rsp = json_decode($smsresult);
			 
			 
	 if($active_log_stock_update){
	 	$directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
		$base  =  $directory->getRoot();
		$logdir=$base .'/var/log/';if(!file_exists($logdir))mkdir($logdir,0777);
		
			if( $dumpFile = @fopen($base .'/var/log/SMSLogin.log', 'a+')){
							 fwrite($dumpFile, date("Y-m-d H:i:s").' : Response SMS data: '.serialize($rsp)."\r\n");
						 }
		 }

	
			$success=false;
			if(isset($rsp->result) && $rsp->result==0)
			$success=true;
			
			
		$this->customerSession->setData('chinaSMSlogin_SmsPhone',$phone);
		$this->customerSession->setData('chinaSMSlogin_SmsCode',$code);
		$this->customerSession->setData('chinaSMSlogin_SmsTime',time());
		
			$result=array();
			if(!$success){
		   $result['status'] = 'invalid';
            $result['message'] = __('Verification code sent error');//.$code
				
			}else{
			 $result = array(
            'status' => 'success',
            'message' => 'Successfully',
       		 );
			}
            $this->getResponse()->setHeader('Content-type', 'application/json');
            $this->getResponse()->setBody($objectManager->create('\Magento\Framework\Json\Helper\Data')->jsonEncode($result));

	return ;
 
    }
	function redirect($url){
		$resultRedirect = $this->resultRedirectFactory->create();
		$resultRedirect->setPath($url);
		return $resultRedirect;
	}
   
}