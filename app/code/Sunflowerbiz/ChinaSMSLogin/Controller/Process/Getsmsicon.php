<?php

namespace Sunflowerbiz\ChinaSMSLogin\Controller\Process;

class Getsmsicon extends \Magento\Framework\App\Action\Action 
{

    /**
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context
    ) {
	
        parent::__construct($context);
		
    }

  
    /**
     * Set redirect
     */
    public function execute()
    {
	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
	$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	$core_write = $resource->getConnection();

	$country_no =  $objectManager->create('Sunflowerbiz\ChinaSMSLogin\Helper\Data')->getConfig('se_chinasmslogin/chinasmslogin/country_no');
	$wait_time =  $objectManager->create('Sunflowerbiz\ChinaSMSLogin\Helper\Data')->getConfig('se_chinasmslogin/chinasmslogin/wait_time');

	$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
	$baseurl=$storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
		$sendurl=$baseurl.'/chinasmslogin/process/smssendcode';
		$smsloginUrl=$baseurl.'/chinasmslogin/process/smslogin';
		
		$html= '
		
		<div class="smslogin " >
		<button onclick="return opensms()" class="button smsloginbutton action primary"><span><span>'.__("SMS Login").'</span></span></button>
		<div class="smslogindiv">
			<div class="countrynodiv">
			 <!--<input type="text" name="countryno" value="+86" class="countryno" placeholder="'.__("Country No.").'">-->
			 <select  name="countryno"  class="countryno">
			 	';
				$countrynos=explode(',',$country_no) ;
				foreach($countrynos as $code){
					$code=trim($code);
					if($code!="")
					$html.= '<option value="'.$code.'">'.$code.'</option>';
				}
				$html.=  '
			 </select>
             </div>
			<div class="phonediv">
			 <input type="text" name="phone" value="" class="phone" placeholder="'.__("Your Phone Number").'">
            </div>
			 <div class="field vcodediv">
                    <input type="text" name="vcode" value="" maxlength="4" class="vcode" placeholder="'.__("Verification Code").'">
                    <button type="button" class="button send action primary" onclick="return smssend(\''.$sendurl.'\')"><span><span>'.__("Send Verification Code").'</span></span></button>
            </div>
				
                <div class="field action" style="margin-top:10px">
                    <div class="newloginbtn">
					<button type="button" class="button login action primary" onclick="return smsloginAction(\''.$smsloginUrl.'\')"><span><span>'.__("Login").'</span></span></button>
					</div>
                </div>
				
				
			<div class="tips">
					<div class="errornumber">'.__("Invalid Phone number").'</div>
					<div class="codesent">'.__("Verification code has sent").'</div>
					<div class="errorphonecode">'.__("Invalid Verification code").'</div>
					<div class="message"></div>
			</div>
		
	<style>

	</style>
	<script>
	var sendcodetext="'.__("Send Verification Code").'";
	var waitcodetext="'.__("Waiting %s seconds").'";
	var initwaitTime = '. (int)$wait_time.';
	var waitTime=initwaitTime;
	var mainUrl = "'.$baseurl.'";

	</script>	
	<div style="clear:both"></div>
		</div>
		</div>
		';

	$this->getResponse()->setBody($html);
	return ;
 
    }

   
}