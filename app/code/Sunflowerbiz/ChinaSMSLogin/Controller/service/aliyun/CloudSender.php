<?php

//https://www.aliyun.com/product/sms
//use Qcloud\Sms\SmsSenderUtil;

/**
 * 单发短信类
 *
 */
 namespace Sunflowerbiz\ChinaSMSLogin\Controller\service\aliyun;
include_once("SmsSenderUtil.php");
class CloudSender
{
    private $url;
    private $appid;
    private $appkey;
    private $util;

    /**
     * 构造函数
     *
     * @param string $appid  sdkappid
     * @param string $appkey sdkappid对应的appkey
     */
	 public function __construct($appid, $appkey)
    {
		   $this->url = "https://dysmsapi.aliyuncs.com";
        $this->appid =  $appid;
        $this->appkey = $appkey;
        $this->util = new SmsSenderUtil();
	}
    public function init($appid, $appkey)
    {
	 $this->url = "https://dysmsapi.aliyuncs.com";
        $this->appid =  $appid;
        $this->appkey = $appkey;
        $this->util = new SmsSenderUtil();
    }

    
    public function sendWithParam($nationCode, $phoneNumber, $templId = 0, $inparams,
        $sign = "", $extend = "", $ext = "")
    {
	
	 $params = array ();

    // *** 需用户填写部分 ***
    // fixme 必填：是否启用https
    $security = false;

    // fixme 必填: 请参阅 https://ak-console.aliyun.com/ 取得您的AK信息
    $accessKeyId =  $this->appid ;
    $accessKeySecret = $this->appkey;

    // fixme 必填: 短信接收号码
    $params["PhoneNumbers"] = $phoneNumber;

    // fixme 必填: 短信签名，应严格按"签名名称"填写，请参考: https://dysms.console.aliyun.com/dysms.htm#/develop/sign
    $params["SignName"] = $sign;

    // fixme 必填: 短信模板Code，应严格按"模板CODE"填写, 请参考: https://dysms.console.aliyun.com/dysms.htm#/develop/template
    $params["TemplateCode"] = $templId;

    // fixme 可选: 设置模板参数, 假如模板中存在变量需要替换则为必填项
	
    $params['TemplateParam'] = Array (
        "code" => $inparams[0]
	 );
	 if(isset( $inparams[1]))
      $params['TemplateParam']["time"] = $inparams[1];
  
	
	  // *** 需用户填写部分结束, 以下代码若无必要无需更改 ***
    if(!empty($params["TemplateParam"]) && is_array($params["TemplateParam"])) {
        $params["TemplateParam"] = json_encode($params["TemplateParam"], JSON_UNESCAPED_UNICODE);
    }

    // 初始化SignatureHelper实例用于设置参数，签名以及发送请求
    $helper =  $this->util;

    // 此处可能会抛出异常，注意catch
	try{
    $content = $helper->request(
        $accessKeyId,
        $accessKeySecret,
        "dysmsapi.aliyuncs.com",
        array_merge($params, array(
            "RegionId" => "cn-hangzhou",
            "Action" => "SendSms",
            "Version" => "2017-05-25",
        )),
        $security
    );
	} catch (\Exception $e) {
   	 $content = json_decode(array("Code"=>'-1'));
	}

	if( $content->Code=='OK') $content->result=0;
	


        return json_encode($content);
    }
}
