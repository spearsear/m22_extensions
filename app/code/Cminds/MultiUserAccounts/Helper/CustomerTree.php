<?php

namespace Cminds\MultiUserAccounts\Helper;

use Magento\Customer\Model\Session\Proxy as CustomerSession;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Cminds\MultiUserAccounts\Model\Config;
use Cminds\MultiUserAccounts\Api\SubaccountTransportRepositoryInterface;

/**
 * Cminds MultiUserAccounts customer helper.
 *
 * @category Cminds
 * @package  Cminds_MultiUserAccounts
 * @author   Stephen Spearsberg <spearsear@gmail.com>
 */

class CustomerTree extends AbstractHelper
{
    protected $logger;

    /**
     * Session object.
     *
     * @var CustomerSession
     */
    private $customerSession;

    /**
     * @var SubaccountTransportRepositoryInterface
     */
    protected $subaccountTransportRepositoryInterface;

    /**
     * Customer Repository Interface
     *
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * Module Config.
     *
     * @var Config
     */
    private $moduleConfig;

    /**
     * Manage constructor.
     *
     * @param Context $context
     * @param CustomerSession $customerSession
     * @param CustomerRepositoryInterface $customerRepository
     * @param SubaccountTransportRepositoryInterface $subaccountTransportRepositoryInterface
     * @param Config $moduleConfig
     */
    public function __construct(
        Context $context,
        CustomerSession $customerSession,
        CustomerRepositoryInterface $customerRepository,
        SubaccountTransportRepositoryInterface $subaccountTransportRepositoryInterface,
        Config $moduleConfig,
	\Psr\Log\LoggerInterface $logger
    ) {
        $this->customerSession = $customerSession;
        $this->customerRepository = $customerRepository;
        $this->subaccountTransportRepositoryInterface = $subaccountTransportRepositoryInterface;
        $this->moduleConfig = $moduleConfig;
        $this->logger = $logger;

        parent::__construct($context);
    }

    /**
     * shuping getRootCustomer
     * 
     * @param int $customerId
     *
     * @return int
     */
    public function getRootCustomerId($customerId) {
        //try to get subaccount based on customerId, if fail, the customerId is a rootCustomer
        $rootCustomerId = 0;
        try {
	    $subaccount = $this->subaccountTransportRepositoryInterface->getByCustomerId($customerId);
            //customer is a subaccount, get its parent customer id
            $parentCustomerId = $subaccount->getParentCustomerId();
            $rootCustomerId = $this->getRootCustomerId($parentCustomerId);
        } catch (\Exception $e) {
            //customer is not a subaccount, then it is a root customer
            $rootCustomerId = $customerId;            
        }
	$this->logger->info("customerId: " . $customerId . " root customerId: " . $rootCustomerId);
        return $rootCustomerId;
    }

}
